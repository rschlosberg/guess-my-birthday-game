from random import randint 

name = input("Hi! What is your name?\n")

guess_number = 1


for i in range(5):
    month_number = randint(1,12)
    year_number = randint(1924, 2004)
    print("Guess", guess_number, ":", name, "were you born on", month_number, "/", year_number, "?")
    answer= input("yes or no?\n")
    if (answer == "yes"):
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Goodbye!")
        exit()
    else:
        print("Drat! Let me try again!")
        guess_number = guess_number + 1
